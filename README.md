
# Distribución espacial de registros publicados a través del SiB Colombia

Este visor geográfico se elaboró como un aporte al Capítulo 2: Estado de la Biodiversidad en Colombia del documento IPBES, en el cual se espacializanlos datos publicados a través del SiB Colombia por medio decuadrículas de 10x10 km, 5 x 5 km y 1 x 1 km. 



# 1. Fuentes de información <a name="id1"></a>


-Esta sección contiene la [ficha metodológica](http://repository.humboldt.org.co/handle/20.500.11761/35288) para la consolidación de las cifras disponibles a través de Biodiversidad en Cifras, donde se detallan los procesos de: I. Consulta de datos a través del SiB Colombia, II. Validación y limpieza de datos, y III. Síntesis de cifras a partir de datos validados y cifras estimadas. A través de esta metodología se procesan los datos disponibles a través del SiB Colombia para obtener cifras que permitan realizar una adecuada gestión del conocimiento sobre la biodiversidad.

## 1.1 Datos sobre biodiversidad <a name="id1"></a>

6’638.860 Registros biológicos (observaciones y especímenes preservados) publicados a través del SiB Colombia con fecha de corte al 31 de Diciembre de 2018. Estos datos representan aproximadamente 51 mil especies con al menos un dato publicado a través del SiB Colombia. Los datos están disponibles de manera libre y gratuita a través de su portal de datos: (datos.biodiversidad.co)[datos.biodiversidad.co]. 


## 1.2 Cartografía <a name="id1"></a>

- Instituto de Investigación de Recursos Biológicos Alexander von Humboldt (2014). Bosque Seco Tropical. Instituto de Investigación de Recursos Biológicos Alexander von Humboldt, Escala 1:100.000. Bogotá D.C., Colombia. Recuperado de: http://geonetwork.humboldt.org.co/geonetwork/srv/spa/search#|eca845f9-dea1-4e86-b562-27338b79ef29

- Instituto de Investigación de Recursos Biológicos Alexander von Humboldt (2016). Ajuste de la capa del componente biótico, incorporada dentro del Mapa Nacional de Ecosistemas Terrestres, Marinos y Costeros de Colombia. Escala 1:100.000. Instituto de Investigación de Recursos Biológicos Alexander von Humboldt. Descripción: Capa ajustada con base en regionalizaciones biogeográficas de Colombia de referencia, como la capa de regiones naturales del IGAC (2002) y las unidades biogeográficas de PNN (2015). Recuperada de: http://geonetwork.humboldt.org.co/geonetwork/srv/spa/search?#|a1afc35c-db98-4110-8093-98e599d1571e


- Instituto de Investigación de Recursos Biológicos Alexander von Humboldt, (2016). Identificación de humedales, escala 1:100.000. Proyecto: Insumos para la delimitación de ecosistemas estratégicos: Páramos y Humedales. Convenio N° 13-014 (FA. 005 de 2013) suscrito entre el Fondo Adaptación y el Instituto Humboldt. Bogotá D.C., Colombia.


- Instituto de Hidrología, Meteorología y Estudios Ambientales (IDEAM), Instituto Alexander von Humboldt (I.Humboldt), Instituto Geográfico Agustín Codazzi (IGAC), Instituto de Investigaciones Marinas y Costeras “José Benito Vives de Andréis” (Invemar) y Ministerio de Ambiente y Desarrollo Sostenible. (2017). Mapa de Ecosistemas Continentales, Costeros y Marinos de Colombia (MEC) [mapa], Versión 2.1, escala 1:100.000.

- Instituto Geográfico Agustín Codazzi. (2013), Mapa de Corporaciones Autónomas Regionales, de Desarrollo Sostenible y Autoridades Ambientales Urbanas, Escala (1:2.500.000). Datum: MAGNA-SIRGAS.

- ANT (2018). Agencia Nacional de Tierras. Resguardos indígenas, Escala. . Bogotá D.C., Colombia. Recuperado de: https://www.datos.gov.co/Agricultura-y-Desarrollo-Rural/Resguardos-Ind-genas/2wvk-ve5b




# 2. Métodología <a name="id1"></a>

Usando el lenguaje python y la librería pandas y geopandas se realizó el cruce espacial de los datos con las fuentes cartográficas. Para la visualización de los resultados en el visor se usó la librería Follium, que hace un binding entre la librería Leaflet de Javascript y python. 
